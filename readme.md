# Rolling logger

A simple rotating log.

## Example

```
$ echo -e '123\n456\n789' | rollog /tmp/test.log

$ cat /tmp/test.log
2018-09-28 14:25:55 - 123
2018-09-28 14:25:55 - 456
2018-09-28 14:25:55 - 789
```

## Usage

```
Usage: rollog [OPTION...] FILE

Options:
    -?                  Show this help doc then exit.
    -t                  Rotate the log file when it's larger than this in
                        bytes.
```
