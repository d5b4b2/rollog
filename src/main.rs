// Copyright 2018 Gerrit Viljoen

// This file is part of rollog.
//
// rollog is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// rollog is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with rollog.  If not, see <http://www.gnu.org/licenses/>.

extern crate chrono;
use chrono::Local;

extern crate getopts;
use getopts::{HasArg, Occur, Options};

use std::env::args;
use std::fmt::{Debug, Display};
use std::fs::{File, OpenOptions};
use std::io::{BufRead, Write, stdin};
use std::path::Path;

const OPTS_SYNTAX: &str = "rollog [OPTION...] FILE";
const HELP_OPT: &str = "?";
const HELP_DESC: &str = "Show this help doc then exit.";
const THRESHOLD_OPT: &str = "t";
const THRESHOLD_DESC: &str = "Rotate the log file when it's larger than this in bytes. Default is 2MB.";

const DEFAULT_THRESHOLD: u64 = 2_000_000;

fn main() -> Result<(), MainError> {
    let opts = options();
    let popts = opts.parse(args().skip(1))?;

    if popts.opt_present(HELP_OPT) {
        print!("Usage: {}", opts.usage(OPTS_SYNTAX));
        return Ok(())
    }

    let threshold = match popts.opt_str(THRESHOLD_OPT) {
        Some(x) => match x.parse::<u64>() {
            Ok(x) => x,
            Err(_) => return err(format!("Invalid option -{}", THRESHOLD_OPT))
        }
        None => DEFAULT_THRESHOLD
    };

    let out_path = match popts.free.first() {
        Some(x) => x,
        None => return err("No log file specified")
    };

    let mut size = {
        let file = OpenOptions::new().create(true).append(true).open(out_path)?;
        file.metadata()?.len()
    };

    let stdin = stdin();
    let stdin = stdin.lock();
    for line in stdin.lines() {
        let line = format!("{} - {}\n", Local::now().format("%F %T"), line?);
        append_str(out_path, &line)?;
        size += line.len() as u64;

        if size > threshold {
            File::create(out_path)?;
            size = 0;
        }
    }
    Ok(())
}

fn append_str<T>(path: T, data: &str) -> ::std::io::Result<()>
where T: AsRef<Path> {
    let mut file = OpenOptions::new().create(true).append(true).open(path)?;
    file.write_all(data.as_bytes())?;
    file.flush()?;
    Ok(())
}

fn options() -> Options {
    let mut res = Options::new();
    res.opt(HELP_OPT, "", HELP_DESC, "", HasArg::No, Occur::Optional);
    res.opt(THRESHOLD_OPT, "", THRESHOLD_DESC, "", HasArg::Yes, Occur::Optional);
    res
}

enum MainError {
    Custom(String),
    Io(::std::io::Error),
    Opts(::getopts::Fail)
}

impl Debug for MainError {

    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        use MainError::*;
        match self {
            Custom(x) => write!(f, "{}", x),
            Io(x) => Debug::fmt(x, f),
            Opts(x) => Debug::fmt(x, f)
        }
    }
}

impl From<::std::io::Error> for MainError {

    fn from(x: ::std::io::Error) -> Self {
        MainError::Io(x)
    }
}

impl From<::getopts::Fail> for MainError {

    fn from(x: ::getopts::Fail) -> Self {
        MainError::Opts(x)
    }
}

fn err<T>(msg: T) -> Result<(), MainError>
where T: Display {
    Err(MainError::Custom(msg.to_string()))
}
